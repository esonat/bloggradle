package com.sonat.blog.dao;

import com.sonat.blog.domain.BlogUser;

public interface UserDao extends GenericDao<BlogUser>{
	BlogUser getUserByName(String name);
	BlogUser getUserByUserName(String username); 
}
