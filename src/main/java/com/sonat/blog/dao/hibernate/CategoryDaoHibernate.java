package com.sonat.blog.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.sonat.blog.dao.CategoryDao;
import com.sonat.blog.domain.Category;
import com.sonat.blog.exception.CategoryNotFoundException;
import com.sonat.blog.util.database.HibernateUtil;

@Repository("categoryDao")
public class CategoryDaoHibernate extends GenericDaoHibernate<Category> implements CategoryDao{
    //protected Log log = LogFactory.getLog(CategoryDaoHibernate.class);

    public CategoryDaoHibernate() {
       super(Category.class);
    }

	@Override
	@SuppressWarnings("unchecked")
	public Category getCategoryByName(String categoryName) throws DataAccessException {
		Session session=HibernateUtil.getSessionFactory().openSession();
		
		Query query= session.createQuery("select cat from Category cat where cat.name = :name");
		query.setParameter("name", categoryName);
		
		List<Category> result=query.list();
		
		if(result==null
				|| result.size()==0) throw new CategoryNotFoundException(0);
		
		session.close();
	    return result.get(0);
	}
}
