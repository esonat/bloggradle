package com.sonat.blog.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sonat.blog.dao.PostDao;
import com.sonat.blog.domain.BlogUser;
import com.sonat.blog.domain.Category;
import com.sonat.blog.domain.Comment;
import com.sonat.blog.domain.Post;
import com.sonat.blog.exception.PostNotFoundException;
import com.sonat.blog.service.UserService;
import com.sonat.blog.util.database.HibernateUtil;
import com.sonat.blog.util.security.SecurityUtilInterface;

@Repository("postDao")
@Transactional
public class PostDaoHibernate extends GenericDaoHibernate<Post> implements PostDao{

	protected Log log = LogFactory.getLog(CommentDaoHibernate.class);
	@Autowired
	private SecurityUtilInterface securityUtil;

	@Autowired
	private UserService userService;

	public PostDaoHibernate() {
		 super(Post.class);
	}

	@Transactional(rollbackFor=DataAccessException.class, readOnly=false, timeout=30, propagation=Propagation.SUPPORTS, isolation=Isolation.DEFAULT)
	public void addPost(Post post,Category category){
		Session session=HibernateUtil.getSessionFactory().openSession();
		
		session.beginTransaction();

		post.setCategory(category);
		post.setDate(new Date());

		Authentication auth = 	SecurityContextHolder.getContext().getAuthentication();
	    String username = 		auth.getName();
		BlogUser user	=		userService.getUserByUsername(username);

		post.setUser(user);
		post.getUser().getPosts().add(post);

		session.save(post);
		session.getTransaction().commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteById(int ID) {
		Session session=HibernateUtil.getSessionFactory().openSession();
		
		Query query	= session.createQuery("FROM Post WHERE ID= :ID");
		query.setParameter("ID", ID);

		Post post=(Post)query.uniqueResult();
		if(post==null) throw new PostNotFoundException(ID);

		session.beginTransaction();

		for(Comment comment:post.getComments()){
			comment.setPost(null);
			session.save(comment);
		}

		session.delete(post);
		session.getTransaction().commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> getAllByDate(Date dateFrom, Date dateTo) {
		Session session=HibernateUtil.getSessionFactory().openSession();
		
		Query query=session.createQuery("FROM Post WHERE date <=:dateTo AND date >= :dateFrom");
		query.setParameter("dateFrom",new java.sql.Date(dateFrom.getTime()));
		query.setParameter("dateTo",new java.sql.Date(dateTo.getTime()));
		
		List<Post> result=query.list(); 
		
		if(result==null ||
			       result.size()==0) return null;
		session.close();
		return (List<Post>)result;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Post> getPostsByCategory(int categoryID) {
		Session session=HibernateUtil.getSessionFactory().openSession();
		
		Query query=session.createQuery("FROM Post P WHERE P.category.ID= :categoryID order by date asc");
		query.setParameter("categoryID", categoryID);
		
		List<Post> result=query.list();
		if(result==null ||
	       result.size()==0) return null;
		session.close();
		return result;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Post> getPostsByUsername(String username) {
		Session session=HibernateUtil.getSessionFactory().openSession();
		
		Query query=session.createQuery("FROM Post P WHERE P.user.username= :username order by date asc");
		query.setParameter("username", username);
	
		List<Post> result=query.list();
		
		if(result==null
			|| result.size()==0) return null;
		session.close();
		return result;
	}



}
