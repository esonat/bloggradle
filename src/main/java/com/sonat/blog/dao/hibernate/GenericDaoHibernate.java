package com.sonat.blog.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.sonat.blog.dao.GenericDao;
import com.sonat.blog.domain.DomainObject;
import com.sonat.blog.util.database.HibernateUtil;


public class GenericDaoHibernate<T extends DomainObject> implements GenericDao<T> {
	private Class<T> type;

    public GenericDaoHibernate(Class<T> type) {
        super();
        this.type = type;
    }

    public void delete(T object) {
    	Session session=HibernateUtil.getSessionFactory().openSession();
    	session.beginTransaction();
    	session.delete(object);
    	session.getTransaction().commit();
    	session.close();
    }

    public T get(int id) {
    	SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
    	Session session=sessionFactory.openSession();
    	
    	T entity= session.get(type,id);
    	sessionFactory.close();
    	
    	return entity;
    }
    
    @SuppressWarnings("unchecked")
	public List<T> getAll() {
    	SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
    	Session session=sessionFactory.openSession();
    	
    	List<T> result=(ArrayList<T>)session.createCriteria(type).list();
    	session.close();
    	return result;
    }

    public void save(T object) {
    	Session session=HibernateUtil.getSessionFactory().openSession();
    	session.beginTransaction();
    	session.save(object);
    	
    	session.getTransaction().commit();
		session.close();
    }


   /* public void indexEntity(T object) {
    	
    	FullTextSession fullTextSession = Search.getFullTextSession(this.getSession()); 
    	fullTextSession.index(object);
    }

    public void indexAllItems() {
        FullTextSession fullTextSession = Search.getFullTextSession(this.getSession());
        ScrollableResults results = fullTextSession.createCriteria(this.type).scroll(ScrollMode.FORWARD_ONLY);
        int counter = 0, numItemsInGroup = 10;
        while (results.next()) {
            fullTextSession.index(results.get(0));
            if (counter++ % numItemsInGroup == 0) {
                fullTextSession.flushToIndexes();
                fullTextSession.clear();
            }
        }
    }
*/
/*
    @Autowired
    public void setupSessionFactory(SessionFactory sessionFactory) {
        this.setSessionFactory(sessionFactory);
    }*/

}
