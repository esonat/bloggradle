package com.sonat.blog.dao.hibernate;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.sonat.blog.dao.UserDao;
import com.sonat.blog.domain.BlogUser;
import com.sonat.blog.exception.UserNotFoundException;
import com.sonat.blog.util.database.HibernateUtil;

@Repository("userDao")
public class UserDaoHibernate extends GenericDaoHibernate<BlogUser> implements UserDao{
	 
	protected Log log = LogFactory.getLog(CommentDaoHibernate.class);
	 
	 public UserDaoHibernate() {
		 super(BlogUser.class);
	 }

	@SuppressWarnings("unchecked")
	@Override
	public BlogUser getUserByName(String name) {
		Session session=HibernateUtil.getSessionFactory().openSession();
		
		Query query=session.createQuery("From BlogUser WHERE name =:name");
		query.setParameter("name", name);
		
		List<BlogUser> result=query.list();
		
		if(result ==null
		|| result.size()==0) {
			throw new UserNotFoundException("No user found with the name: "+ name);
		}
		
		session.close();
		return result.get(0);
	}

	 @SuppressWarnings("unchecked")
	 @Override
	 public BlogUser getUserByUserName(String username) {
		Session session=HibernateUtil.getSessionFactory().openSession();
			
		Query query=session.createQuery("From BlogUser where username= :username");
		query.setParameter("username", username);
		
		List<BlogUser> result=query.list();
		
		if(result==null
		|| result.size()==0){
			throw new UserNotFoundException("No user found with the username: "+username);
		}
		
		session.close();
		return result.get(0);
	}
}
