package com.sonat.blog.dao;

import java.util.List;

import com.sonat.blog.domain.DomainObject;

public interface GenericDao<T extends DomainObject> {
	public void delete(T object);

    public T get(int id);

    public List<T> getAll();

    public void save(T object);

/*    public void indexEntity(T object);

    public void indexAllItems();*/
}
