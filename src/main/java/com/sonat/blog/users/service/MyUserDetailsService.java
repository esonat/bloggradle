package com.sonat.blog.users.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.sonat.blog.dao.UserDao;
import com.sonat.blog.domain.UserRole;

@Component
public class MyUserDetailsService implements UserDetailsService {
	@Autowired
	private UserDao userDao;

	private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		for (UserRole userRole : userRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
		}

		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);
		return Result;
	}

	private User buildUserForAuthentication(com.sonat.blog.domain.BlogUser user, List<GrantedAuthority> authorities) {
		return new User(user.getUsername(), user.getPassword(), user.isEnabled(), true, true, true, authorities);
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		
		com.sonat.blog.domain.BlogUser user = userDao.getUserByUserName(username);
		List<GrantedAuthority> authorities = buildUserAuthority(user.getUserRole());

		return buildUserForAuthentication(user, authorities);
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}