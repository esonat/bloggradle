package com.sonat.blog.util.datetime;

public class DateTimeConstants {
	public static final String DATE_FORMAT		=	"yyyy-MM-dd";
	public static final String DATETIME_FORMAT	=	"yyyy-MM-ddHH:mm:ss";	
}
