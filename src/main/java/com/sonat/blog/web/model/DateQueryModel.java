package com.sonat.blog.web.model;

public class DateQueryModel {
	private String dateFrom;
	private DateQueryEnum dateQueryEnum;
	private String dateTo;
	
	public DateQueryModel(String dateFrom,String dateTo,DateQueryEnum dateQueryEnum){
		this.dateFrom=dateFrom;
		this.dateTo=dateTo;
		this.dateQueryEnum=dateQueryEnum;
	}
	
	public String getDateFrom() {
		return dateFrom;
	}
	public DateQueryEnum getDateQueryEnum() {
		return dateQueryEnum;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public void setDateQueryEnum(DateQueryEnum dateQueryEnum) {
		this.dateQueryEnum = dateQueryEnum;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	
	
}
