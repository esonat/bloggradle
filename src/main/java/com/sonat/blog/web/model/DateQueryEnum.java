package com.sonat.blog.web.model;

public enum DateQueryEnum {
	thismonth,
	thisweek,
	thisyear,
	today
};
