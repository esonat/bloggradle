package com.sonat.blog.config;

import java.util.Properties;

import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

@Configuration
@ComponentScan("com.sonat.blog.dao.hibernate")
//@EnableTransactionManagement
public class HibernateConfig {
	
	/*@Autowired
	private BasicDataSource blogDataSource;
	*/

	public DataSource blogDataSource(){
		BasicDataSource basicDataSource=new BasicDataSource();
		basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
		basicDataSource.setUrl("jdbc:mysql://localhost:3306/blogDB");
		basicDataSource.setUsername("root");
		basicDataSource.setPassword("sonat");
		
		return basicDataSource;
	}
	
	
	@Bean
	 public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor(){
		return new PersistenceExceptionTranslationPostProcessor();
	}
	
/*	@Bean 
	public DefaultLobHandler defaultLobHandler(){
		return new DefaultLobHandler();
	}*/
	/*@Bean 
	@Lazy
	public HibernateTransactionManager transactionManager(){
		HibernateTransactionManager transactionManager
		=new HibernateTransactionManager();
		transactionManager.setSessionFactory(sessionFactory().getObject());
		
		return transactionManager;
	}*/
	
	@Bean 
	public LocalSessionFactoryBean sessionFactory(){
		LocalSessionFactoryBean annotationSessionFactoryBean
			=new LocalSessionFactoryBean();
		annotationSessionFactoryBean.setDataSource(blogDataSource());
		//annotationSessionFactoryBean.setLobHandler(defaultLobHandler());
		annotationSessionFactoryBean.setPackagesToScan("com.sonat.blog.domain");
		//annotationSessionFactoryBean.setAnnotatedPackages("com.sonat.blog.domain");
		
		Properties props=new Properties();
		props.put("hibernate.dialect","${hibernate.dialect}");
		props.put("hibernate.jdbc.batch_size","${hibernate.jdbc.batch_size}");
		props.put("hibernate.c3p0.max_size","${hibernate.c3p0.max_size}");
		props.put("hibernate.c3p0.min_size","${hibernate.c3p0.min_size}");
		props.put("hibernate.c3p0.timeout","${hibernate.c3p0.timeout}");
		props.put("hibernate.c3p0.max_statements","${hibernate.c3p0.max_statements}");
		props.put("hibernate.c3p0.idle_test_period","${hibernate.c3p0.idle_test_period}");
		props.put("hibernate.c3p0.acquire_increment","${hibernate.c3p0.acquire_increment}");
		props.put("hibernate.c3p0.validate","${hibernate.c3p0.validate}");
		//props.put("hibernate.cache.provider_class","${hibernate.cache.provider_class}");
		props.put("hibernate.connection.provider_class","${hibernate.connection.provider_class}");
		props.put("hibernate.show_sql","${hibernate.show_sql}");
		props.put("hibernate.hbm2ddl.auto","update");
		//props.put("hibernate.cache.use_second_level_cache","true");
		//props.put("hibernate.cache.provider_class","net.sf.ehcache.hibernate.SingletonEhCacheProvider");
		//props.put("hibernate.cache.use_query_cache","true");
		//props.put("hibernate.cache.region.factory_class", "org.hibernate.cache.EhCacheRegionFactory");
		props.put("hibernate.search.default.directory_provider","${hibernate.search.default.directory_provider}");
		props.put("hibernate.search.default.indexBase","${hibernate.search.default.indexBase}");
		props.put("hibernate.search.default.batch.merge_factor","${hibernate.search.default.batch.merge_factor}");
		props.put("hibernate.search.default.batch.max_buffered_docs","${hibernate.search.default.batch.max_buffered_docs}");
		props.put("hibernate.current_session_context_class","thread");
		
		annotationSessionFactoryBean.setHibernateProperties(props);

		return annotationSessionFactoryBean;
	}
	 
	
	
}
