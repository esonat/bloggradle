package com.sonat.blog.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:META-INF/spring/jdbc.properties")
public class DatasourceConfig {
	
	@Bean
	public BasicDataSource blogDataSource(){
		BasicDataSource basicDataSource=new BasicDataSource();
		basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
		basicDataSource.setUrl("jdbc:mysql://localhost:3306/blogDB");
		basicDataSource.setUsername("root");
		basicDataSource.setPassword("sonat");
		
		return basicDataSource;
	}
}
