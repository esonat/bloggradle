package com.sonat.blog.config.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.sonat.blog.config.BlogBeanConfiguration;
import com.sonat.blog.users.service.MyUserDetailsService;

@Configuration
@EnableWebSecurity
@Import({BlogBeanConfiguration.class})
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private MyUserDetailsService myUserDetailsService;
	
	@Bean 
	public BCryptPasswordEncoder bCryptPasswordEncoder(){
		BCryptPasswordEncoder bCryptPasswordEncoder
			=new BCryptPasswordEncoder(11);
		
		return bCryptPasswordEncoder;
	}	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception{		
		auth
			.userDetailsService(myUserDetailsService)
			.passwordEncoder(bCryptPasswordEncoder());		
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http
			.authorizeRequests()
			.antMatchers("/post/add*").hasAnyRole("ADMIN","USER")
			.antMatchers("/category/add*").hasAnyRole("ADMIN")
			.antMatchers("/post/delete*").hasRole("ADMIN")
		.and()
			.formLogin()
			.loginPage("/login")
			.defaultSuccessUrl("/post")
			.failureUrl("/login?error")
			.usernameParameter("username")
			.passwordParameter("password")
		.and()
		.logout()
			.logoutSuccessUrl("/login?logout");
	}
	
}
