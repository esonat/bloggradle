package com.sonat.blog.config.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.sonat.blog.users.service.MyUserDetailsService;

@Configuration
@EnableWebMvc
public class WebDatabaseConfig {
	@Bean 
	public MyUserDetailsService myUserDetailsService(){
		return new MyUserDetailsService();
	}
}
