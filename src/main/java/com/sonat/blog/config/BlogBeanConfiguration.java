package com.sonat.blog.config;

import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;

import com.sonat.blog.dao.hibernate.UserDaoHibernate;
import com.sonat.blog.users.service.MyUserDetailsService;

@Configuration
@Import({DatasourceConfig.class,
		 //JpaConfig.class,
		 HibernateConfig.class})
@ComponentScan("com.sonat.blog")
public class BlogBeanConfiguration {
	
	@Bean
	public DozerBeanMapperFactoryBean dozerBeanMapperFactoryBean(){
		DozerBeanMapperFactoryBean dozerBeanMapperFactoryBean=
				new DozerBeanMapperFactoryBean();
		dozerBeanMapperFactoryBean.setMappingFiles(new Resource[]{new ClassPathResource("classpath*:/*mapping.xml")});
		return dozerBeanMapperFactoryBean;
	}
	
	/*@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
      return new PropertySourcesPlaceholderConfigurer();
	}*/
	
	@Bean
	public MyUserDetailsService myUserDetailsService(){
		MyUserDetailsService myUserDetailsService=new MyUserDetailsService();
		myUserDetailsService.setUserDao(userDao());
		
		return myUserDetailsService;
	}
	
	@Bean
	public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor(){
		return new PersistenceExceptionTranslationPostProcessor();
	}
	@Bean 
	public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer(){
		PropertyPlaceholderConfigurer propertyPlaceholderConfigurer=new PropertyPlaceholderConfigurer();
		propertyPlaceholderConfigurer.setLocation(
										new ClassPathResource("META-INF/spring/jdbc.properties"));
		return propertyPlaceholderConfigurer;
	}
	
	@Bean
	public UserDaoHibernate userDao(){
		return new UserDaoHibernate();
	}

}
