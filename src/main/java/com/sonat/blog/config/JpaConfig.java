/*package com.sonat.blog.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.validator.internal.metadata.raw.BeanConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement(mode=AdviceMode.ASPECTJ)
public class JpaConfig {
	
	
	public DataSource blogDataSource(){
		BasicDataSource basicDataSource=new BasicDataSource();
		basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
		basicDataSource.setUrl("jdbc:mysql://localhost:3306/blogDB");
		basicDataSource.setUsername("root");
		basicDataSource.setPassword("sonat");
		
		return basicDataSource;
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(){
		LocalContainerEntityManagerFactoryBean entityManagerFactory
			=new LocalContainerEntityManagerFactoryBean();
		
		entityManagerFactory.setDataSource(blogDataSource());
		return entityManagerFactory;
	}
	@Bean 
	public PersistenceAnnotationBeanPostProcessor persistenceAnnotationBeanPostProcessor(){
		return new PersistenceAnnotationBeanPostProcessor();
	}
	
	@Bean 
	public DefaultLobHandler defaultLobHandler(){
		return new DefaultLobHandler();
	}
	
	@Bean 
	public JpaTransactionManager transactionManager(){
		JpaTransactionManager jpaTransactionManager=
				new JpaTransactionManager();
		
		jpaTransactionManager.setEntityManagerFactory(entityManagerFactory().getNativeEntityManagerFactory());
		return jpaTransactionManager;
	}
	
}
*/