package com.sonat.blog.config;

import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.sonat.blog.config.web.WebConfig;
import com.sonat.blog.config.web.WebDatabaseConfig;
import com.sonat.blog.config.web.WebSecurityConfig;

@Order(1)
public class BlogWebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{
	
	protected Class<?>[] getRootConfigClasses(){
		return new Class<?>[] {BlogBeanConfiguration.class,WebSecurityConfig.class,WebDatabaseConfig.class};		
	}
	
	protected Class<?>[] getServletConfigClasses(){
		return new Class<?>[] {WebConfig.class};	
	}
	
	protected String[] getServletMappings(){
		return new String[] {"/"};
		
	}
}
