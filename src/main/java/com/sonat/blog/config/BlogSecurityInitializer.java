package com.sonat.blog.config;

import javax.servlet.Filter;

import org.springframework.core.annotation.Order;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

@Order(2)
public class BlogSecurityInitializer extends  AbstractSecurityWebApplicationInitializer {
	
	protected Filter[] getServletFilters() {
		OpenEntityManagerInViewFilter viewFilter=new OpenEntityManagerInViewFilter();
		
		return new Filter[]{viewFilter};
	}
}