package com.sonat.blog.rest.resource;

public class ErrorMessages {
	public static final String CATEGORY_NOT_FOUND	="Category not found";
	public static final String COMMENT_NOT_FOUND	="Comment not found";
	public static final String POST_NOT_FOUND		="Post not found";
	public static final String USER_NOT_FOUND		="User not found";

}
