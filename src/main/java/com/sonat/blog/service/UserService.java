package com.sonat.blog.service;

import java.util.List;

import com.sonat.blog.domain.BlogUser;

public interface UserService {
	void addUser(BlogUser user);
	List<BlogUser> getAll();
	BlogUser getUserByName(String name);
	BlogUser getUserByUsername(String username);
}
