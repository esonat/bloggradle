package com.sonat.blog.service;

import java.util.List;

import com.sonat.blog.domain.Category;

public interface CategoryService {
	void			addCategory		(Category category);	
	void			deleteCategory	(int categoryID);	
	List<Category> 	getAllCategories();	
	Category 		getCategoryById	(int categoryID);
	Category 		getCategoryByName(String categoryName);
}
