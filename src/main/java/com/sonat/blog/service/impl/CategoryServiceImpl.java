package com.sonat.blog.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sonat.blog.dao.CategoryDao;
import com.sonat.blog.domain.Category;
import com.sonat.blog.exception.CategoryNotFoundException;
import com.sonat.blog.service.CategoryService;

@Service(value = "categoryService")
@Transactional
public class CategoryServiceImpl implements CategoryService{
	@Autowired
	private CategoryDao categoryDao;

	@Transactional(rollbackFor=DataAccessException.class, readOnly=false, timeout=30, propagation=Propagation.SUPPORTS, isolation=Isolation.DEFAULT)
	public void	addCategory(Category category) throws DataAccessException{
		categoryDao.save(category);
	}
	public void	deleteCategory(int categoryID)
	throws DataAccessException{
		Category category=getCategoryById(categoryID);
		categoryDao.delete(category);
	}
	public List<Category> getAllCategories(){
		List<Category> list;
		try{
			list=categoryDao.getAll();
		}catch(Exception e){
			return null;
		}
		return list;
	}

	public Category getCategoryById	(int categoryID){
		if(categoryDao.get(categoryID)==null)
			throw new CategoryNotFoundException(categoryID);

		return categoryDao.get(categoryID);
	}

	public Category getCategoryByName(String categoryName)
	throws CategoryNotFoundException{
		return categoryDao.getCategoryByName(categoryName);
	}
}
