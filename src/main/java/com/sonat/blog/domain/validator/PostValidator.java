package com.sonat.blog.domain.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.sonat.blog.domain.Post;
import com.sonat.blog.service.PostService;

@Component
public class PostValidator implements Validator{
	@Autowired
	private PostService postService;
	
	@SuppressWarnings("rawtypes")
	public boolean supports(Class clazz){
		return Post.class.isAssignableFrom(clazz);
	}
	public void validate(Object target,Errors errors){
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"text","error.text","Text is required");
		Post post=(Post)target;
		if(post.getText().length()<5
		|| post.getText().length()>1000000)
			errors.rejectValue("text","Size.Post.text.validation");
		
		if(postService.getAll()!=null){
			for(Post item:postService.getAll()){
				if(post.getText().equals(item.getText()))
				errors.rejectValue("text", "Unique.Post.text.validation");
			}		
		}
	}
}
