package com.sonat.blog.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;

import com.sonat.blog.validator.Name;
import com.sonat.blog.validator.Username;


@SuppressWarnings("serial")
@Entity
@Table(name="user",catalog="blogDB")
public class BlogUser implements DomainObject{
	private boolean enabled;
	private int ID;
	@Pattern(regexp="[a-zA-Z]+",message="{Pattern.User.Name.validation}")
	@Name
	private String name;
	//@Pattern(regexp="^[A-Za-z0-9_.]+$",message="{Pattern.User.password.validation}")
	@Size(min=5,max=150,message="{Size.User.password.validation}")
	private String password;
	private Set<Post> posts=new HashSet<Post>(0);
	@Pattern(regexp="[a-zA-Z]+",message="{Pattern.User.username.validation}")
	@Username
	private String username;
	private Set<UserRole> 	userRole = new HashSet<UserRole>(0);
    //private Integer version;

    public BlogUser() {
		super();
	}
	public BlogUser(String name) {
		this.name=name;
	}
	public BlogUser(String name,String username, String password, boolean enabled) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
	}

	public BlogUser(String name,String username, String password, boolean enabled, Set<UserRole> userRole) {
		this.name=name;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.userRole = userRole;
	}

	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="USER_ID",unique=true,nullable=false)
	@JsonIgnore
	public int getID() {
		return ID;
	}

	@Column(name="NAME",unique=true,nullable=false)
	public String getName() {
		return name;
	}
	@Column(name="PASSWORD",nullable=false)
	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@OneToMany(fetch=FetchType.EAGER,mappedBy="user")
	@Cascade(org.hibernate.annotations.CascadeType.DELETE)
	@JsonIgnore
	public Set<Post> getPosts() {
		return posts;
	}
	@Column(name="USERNAME",unique=true,nullable=false)
	public String getUsername() {
		return username;
	}

	@OneToMany(fetch=FetchType.EAGER,mappedBy="user")
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	public Set<UserRole> getUserRole() {
		return userRole;
	}
	@Column(name="ENABLED",nullable=false)
	@JsonIgnore
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public void setID(int iD) {
		ID = iD;
	}

	public void setName(String name) {
		this.name = name;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}

/*	@Version
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }*/


}
